package com.cursospring.batch.taskletkafka.job;

import com.cursospring.batch.taskletkafka.dto.EmployeeDTO;
import com.cursospring.batch.taskletkafka.mapper.EmployeeFileRowMapper;
import com.cursospring.batch.taskletkafka.model.Employee;
import com.cursospring.batch.taskletkafka.processor.EmployeeProcessor;
import com.cursospring.batch.taskletkafka.sender.EmployeeKafkaSender;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import static com.cursospring.batch.taskletkafka.utils.Constants.AGE;
import static com.cursospring.batch.taskletkafka.utils.Constants.CHUNK_SIZE;
import static com.cursospring.batch.taskletkafka.utils.Constants.EMAIL;
import static com.cursospring.batch.taskletkafka.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.taskletkafka.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.taskletkafka.utils.Constants.JOB_NAME;
import static com.cursospring.batch.taskletkafka.utils.Constants.LASTNAME;
import static com.cursospring.batch.taskletkafka.utils.Constants.STEP_NAME;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EmployeeProcessor employeeProcessor;

    @Qualifier(value = JOB_NAME)
    @Bean
    public Job demoOneJob() throws Exception {
        return jobBuilderFactory.get(JOB_NAME).start(stepOneDemo()).build();
    }

    @Bean
    public Step stepOneDemo() throws Exception {
        return stepBuilderFactory.get(STEP_NAME)
                .<EmployeeDTO, Employee>chunk(CHUNK_SIZE)
                .reader(employeeReader())
                .processor(employeeProcessor)
                .writer(employeeKafkaSender())
                .build();
    }

    @Bean
    @StepScope
    public Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
        return new ClassPathResource(fileName);
    }

    @Bean
    @StepScope
    public FlatFileItemReader<EmployeeDTO> employeeReader() throws Exception {
        FlatFileItemReader<EmployeeDTO> reader = new FlatFileItemReader<>();
        reader.setResource(inputFileResource(null));
        reader.setLineMapper(defineMapper());
        return reader;
    }

    private DelimitedLineTokenizer defineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(EMPLOYEE_ID, FIRSTNAME, LASTNAME, EMAIL, AGE);
        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        return lineTokenizer;
    }

    private DefaultLineMapper<EmployeeDTO> defineMapper() {
        DefaultLineMapper<EmployeeDTO> lineMapper = new DefaultLineMapper<>();
        lineMapper.setLineTokenizer(defineTokenizer());
        lineMapper.setFieldSetMapper(new EmployeeFileRowMapper());
        return lineMapper;
    }

    @Bean
    public EmployeeKafkaSender employeeKafkaSender() {
        return new EmployeeKafkaSender();
    }
}
