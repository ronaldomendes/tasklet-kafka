package com.cursospring.batch.taskletkafka.utils;

public class Constants {

    public static final String JOB_NAME = "demoOne";
    public static final String STEP_NAME = "stepOne";
    public static final int CHUNK_SIZE = 1;
    public static final String TOPIC_NAME = "TOPIC_TEST";
    public static final String CONTEXT_KEY_NAME = "fileName";
    public static final String FILE_NAME_CSV = "employees.csv";

    public static final String EMPLOYEE_ID = "employeeId";
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String EMAIL = "email";
    public static final String AGE = "age";

    public static final String HOST = "localhost:9002";
    public static final String MAX_POLL = "654321";

    private Constants() {
    }
}
