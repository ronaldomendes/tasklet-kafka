package com.cursospring.batch.taskletkafka.sender;

import com.cursospring.batch.taskletkafka.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Slf4j
public class EmployeeKafkaSender implements ItemWriter<Employee> {

    @Autowired
    private MessageSender sender;

    @Override
    public void write(List<? extends Employee> employees) throws Exception {
        employees.forEach(sender::send);
        log.info("Sending message to Kafka");
    }
}
