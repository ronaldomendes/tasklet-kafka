package com.cursospring.batch.taskletkafka.sender;

import com.cursospring.batch.taskletkafka.model.Employee;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static com.cursospring.batch.taskletkafka.utils.Constants.TOPIC_NAME;

@Service
@AllArgsConstructor
public class MessageSender {

    private KafkaTemplate<String, Employee> kafkaTemplate;

    public void send(Employee employee) {
        kafkaTemplate.send(TOPIC_NAME, employee);
    }
}
